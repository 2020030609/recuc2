var total = 0;
var numR = 0;

function Generar(){
    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
    var numEdad = getRandomIntInclusive(18,99);
    document.getElementById('edad').value = numEdad;
    document.getElementById('altura').value = (Math.random() * (2.5 - 1.5) + 1.5).toFixed(2);
	document.getElementById('peso').value = (Math.random() * (130 - 20) + 20).toFixed(2);    
}

function Calcular(){
    let peso = document.getElementById('peso').value; 
    let altura = document.getElementById('altura').value;
    var indice = peso / Math.pow(altura, 2);

    if (indice < 18.50){
        document.getElementById('nivel').value = "Bajo peso";
    }else if (indice >= 18.50 && indice <= 24.90){
        document.getElementById('nivel').value = "Peso saludable";
    }else if (indice >= 25.00 && indice <= 29.90){
        document.getElementById('nivel').value = "Sobrepeso";
    }else if (indice >= 30.00){
        document.getElementById('nivel').value = "Obesidad";
    }
    document.getElementById('imc').value = indice.toFixed(2);
}

function Registro(){
    var imcConvert = parseFloat(document.getElementById("imc").value);
	numR += 1;
	total += imcConvert;
	document.getElementById('imcProm').innerText = (total / numR).toFixed(2);
    let registros = document.getElementById('pRegistros');

	registros.innerHTML += `<tr>
		<td>${numR}</td>
		<td>${document.getElementById('edad').value}</td>
		<td>${document.getElementById('altura').value}</td>
		<td>${document.getElementById('peso').value}</td>
		<td>${document.getElementById('imc').value}</td>
		<td>${document.getElementById('nivel').value}</td>
	</tr>`;

}

function Borrar(){
    document.getElementById('pRegistros').innerText = "";
    document.getElementById('imcProm').innerText = "";
}